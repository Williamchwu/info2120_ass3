CREATE FUNCTION Update_Nbooked_trig() RETURNS trigger AS
  $$
  BEGIN
  IF (TG_OP = 'DELETE') THEN
    UPDATE Journey SET nbooked=nbooked-1
    WHERE journey_id = OLD.journey_id;
    RETURN OLD;
  ELSIF (TG_OP = 'INSERT') THEN
    UPDATE Journey SET nbooked=nbooked+1
    WHERE journey_id = NEW.journey_id;
    RETURN NEW;
  ELSIF (TG_OP = 'UPDATE') THEN
    UPDATE Journey SET nbooked=nbooked-1
    WHERE journey_id = OLD.journey_id;
    UPDATE Journey SET nbooked=nbooked+1
    WHERE journey_id = NEW.journey_id;
    RETURN NEW;
  END IF;
  RETURN NULL;
  END
  $$
  LANGUAGE plpgsql;

CREATE TRIGGER Update_Nbooked
AFTER INSERT OR UPDATE OR DELETE ON booking
FOR EACH ROW
EXECUTE PROCEDURE Update_Nbooked_trig();


CREATE FUNCTION Vehicle_capacity_check() RETURNS trigger AS
  $$
  BEGIN
  IF((
    SELECT nbooked
    FROM Journey J
    WHERE journey_id = NEW.journey_id)
    >=
    (SELECT capacity
     FROM Vehicle V JOIN Journey USING(vehicle_code)
     WHERE journey_id = NEW.journey_id
 )) THEN
    IF(TG_OP = 'DELETE') THEN
      RETURN OLD;
    ELSIF(TG_OP = 'UPDATE') THEN
      RETURN OLD;
    END IF;
  ELSE
    RETURN NEW;
  END IF;
  RETURN NULL;
  END
  $$
  LANGUAGE plpgsql;


CREATE TRIGGER Capacity_Check
BEFORE INSERT OR UPDATE ON booking
FOR EACH ROW
EXECUTE PROCEDURE Vehicle_capacity_check();


--DROP TRIGGER IF EXISTS Update_Nbooked ON booking;
--DROP TRIGGER IF EXISTS Capacity_Check ON booking;
--DROP FUNCTION IF EXISTS Update_Nbooked_trig();
--DROP FUNCTION IF EXISTS Vehicle_capacity_check();
