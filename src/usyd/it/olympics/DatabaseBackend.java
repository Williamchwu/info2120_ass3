package usyd.it.olympics;


/**
 * Database back-end class for simple gui.
 * 
 * The DatabaseBackend class defined in this file holds all the methods to 
 * communicate with the database and pass the results back to the GUI.
 *
 *
 * Make sure you update the dbname variable to your own database name. You
 * can run this class on its own for testing without requiring the GUI.
 */
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

/**
 * Database interfacing backend for client. This class uses JDBC to connect to
 * the database, and provides methods to obtain query data.
 * 
 * Most methods return database information in the form of HashMaps (sets of 
 * key-value pairs), or ArrayLists of HashMaps for multiple results.
 *
 * @author Bryn Jeffries {@literal <bryn.jeffries@sydney.edu.au>}
 */
public class DatabaseBackend {

    ///////////////////////////////
    /// DB Connection details
	/// These are set in the constructor so you should never need to read or 
	/// write to them yourself 
    ///////////////////////////////
    private final String dbUser;
    private final String dbPass;
	private final String connstring;


    ///////////////////////////////
    /// Student Defined Functions
    ///////////////////////////////

    /////  Login and Member  //////

    /**
     * Validate memberID details
     * 
     * Implements Core Functionality (a)
     *
     * @return basic details of user if username is for a valid memberID and password is correct
     * @throws OlympicsDBException 
     * @throws SQLException
     */
    public HashMap<String,Object> checkLogin(String member, char[] password) throws OlympicsDBException  {
    	HashMap<String,Object> details = null;
    	Connection conn = null;
        try {
            conn = getConnection();
            conn.setTransactionIsolation(8);
            
            conn.setAutoCommit(false);
            //CallableStatement cstmt = conn.prepareCall(“call CHECK_LOGIN(member, new String(password))”);
            String query = "SELECT * FROM Member WHERE member_id = ? AND pass_word = ?;";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, member);
            stmt.setString(2, new String(password));
            ResultSet rset = stmt.executeQuery();
            if(!rset.next()){
            	rset.close();
            	conn.commit();
            	conn.close();
            }else{
            	details = new HashMap<String,Object>();
            	details.put("member_id", member);
            	details.put("title", rset.getString("title"));
            	details.put("first_name", rset.getString("given_names"));
            	details.put("family_name", rset.getString("family_name"));
            	details.put("country_name", rset.getString("country_code"));
            	details.put("residence", rset.getInt("accommodation"));
            	if(checkMemberType(conn, member, "Athlete")){
                	details.put("member_type", "athlete");
                }else if(checkMemberType(conn, member, "Official")){
                	details.put("member_type", "official");
                }else if(checkMemberType(conn, member, "Staff")){
                	details.put("member_type", "staff");
                }
            	rset.close();
            	conn.commit();
            	conn.close();
            }     
        } catch (Exception e) {
        	try{
        		if(conn != null){
        		conn.rollback();
        		conn.close();
        		}
        	}catch(SQLException e1){
        		throw new OlympicsDBException("SQL Rollback exception", e1);
        	}
            throw new OlympicsDBException("Error checking login details", e);
        }
        return details;
    }

    /**
     * Obtain details for the current memberID
     * @param memberID 
     *
     * @return Details of member
     * @throws OlympicsDBException
     * @throws SQLException 
     */
    public HashMap<String, Object> getMemberDetails(String memberID) throws OlympicsDBException {
    	HashMap<String, Object> details = new HashMap<String, Object>();
    	Connection conn = null;
    	try{
    		conn = getConnection();
            conn.setTransactionIsolation(8);
            
            conn.setAutoCommit(false);

        	String query = "SELECT * FROM Member WHERE member_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, memberID);
            ResultSet rset = stmt.executeQuery();
            if(rset.next()){
            	details.put("member_id", memberID);
                details.put("title", rset.getString("title"));
             	details.put("first_name", rset.getString("given_names"));
               	details.put("family_name", rset.getString("family_name"));
               	if(checkMemberType(conn, memberID, "Athlete")){
                	details.put("member_type", "athlete");
                	// This is for an athlete
                	details.put("num_gold", countMedal(conn, memberID, "G"));
                	details.put("num_silver", countMedal(conn, memberID, "S"));
                	details.put("num_bronze", countMedal(conn, memberID, "B"));
                }else if(checkMemberType(conn, memberID, "Official")){
                	details.put("member_type", "official");
                }else if(checkMemberType(conn, memberID, "Staff")){
                	details.put("member_type", "staff");
                }
               	// Put Residence Name
               	details.put("residence", getResidenceName(conn, rset.getInt("accommodation")));
               	rset.close();
               	
               	// Put Country Name
            	String query_country_name = "SELECT Country.country_name "
               			+ "FROM Country NATURAL JOIN Member "
               			+ "WHERE member_id = ?;";
                stmt = conn.prepareStatement(query_country_name);
                stmt.setString(1, memberID);
                rset = stmt.executeQuery();
                if(rset.next()){
                   	details.put("country_name", rset.getString("country_name"));
                }	
               	rset.close();
               	
               	// Put num_bookings
               	String query_num_bookings = "SELECT count(*) "
               			+ "FROM member JOIN Booking ON (member.member_id = Booking.booked_for) "
               			+ "WHERE member_id = ?;";
                stmt = conn.prepareStatement(query_num_bookings);
                stmt.setString(1, memberID);
                rset = stmt.executeQuery();
                if(rset.next()){
                   	details.put("num_bookings", rset.getInt("count"));
                }
                rset.close();
                conn.commit();
                conn.close();
                return details;
            }else{
            	conn.commit();
                conn.close();
            	return null;
            }            
    	}catch (Exception e) {
    		try{
        		if(conn != null){
        		conn.rollback();
        		conn.close();
        		}
        	}catch(SQLException e1){
        		throw new OlympicsDBException("SQL Rollback exception", e1);
        	}
            throw new OlympicsDBException(e.getMessage()); //Error getting member details
        }
    }
    
    private int countMedal(Connection conn, String athlete_id, String modeltype) throws OlympicsDBException{
    	try {
    		String query_medal = "SELECT count(medal) "+
        			"FROM participates "+
        			"WHERE athlete_id = ? AND medal = ?";
        	PreparedStatement stmt = conn.prepareStatement(query_medal);
            stmt.setString(1, athlete_id);
            stmt.setString(2, modeltype);
            ResultSet rset = stmt.executeQuery();
            rset.next();
            int countIndividual = rset.getInt("count");
            rset.close();
            
            query_medal = "SELECT count(medal) "+
               "FROM Team natural join TeamMember "+
               "WHERE athlete_id = ? AND medal = ?";
            stmt = conn.prepareStatement(query_medal);
            stmt.setString(1, athlete_id);
            stmt.setString(2, modeltype);
            rset = stmt.executeQuery();
            rset.next();
            int countTeam = rset.getInt("count");
            int count = countIndividual + countTeam;
            rset.close();
            
            conn.commit();
            
            return count;
    	}catch (Exception e) {
    		try{
        		if(conn != null){
        		conn.rollback();
        		conn.close();
        		}
        	}catch(SQLException e1){
        		throw new OlympicsDBException("SQL Rollback exception", e1);
        	}
            throw new OlympicsDBException("Error getting medal details", e);
    	}
    }
    
    private boolean checkMemberType(Connection conn, String MemberID, String type) throws OlympicsDBException{
    	try{
            //CallableStatement cstmt = conn.prepareCall(“{call CHECK_TYPE(?, ?, ?)”});
            //cstmt.setString(1, MemberID);
            //cstmt.setString(2, type);
            //cstmt.registerOutParmeter(3, Types.INTEGER);
            //cstmt.executeUpdate();
            //cstmt.getInt(3);
    		String query = "SELECT * FROM " + type + " WHERE member_id = ?";
    		PreparedStatement stmt = conn.prepareStatement(query);
    		stmt.setString(1, MemberID);
    		ResultSet rset = stmt.executeQuery();
    		if(rset.next()){
    			rset.close();
    			conn.commit();
    			return true;
    		}else{
    			rset.close();
    			conn.commit();
    			return false;
    		}
    	}catch (Exception e) {
    		try{
        		if(conn != null){
        		conn.rollback();
        		conn.close();
        		}
        	}catch(SQLException e1){
        		throw new OlympicsDBException("SQL Rollback exception", e1);
        	}
            throw new OlympicsDBException("Error checking member type", e);
        }
    }
    
    public String getResidenceName(Connection conn, int i) throws OlympicsDBException{
    	try{
        	String query = "SELECT * FROM Place WHERE place_id = ?";
        	PreparedStatement stmt = conn.prepareStatement(query);
        	stmt.setInt(1, i);
        	ResultSet rset = stmt.executeQuery();
        	if(rset.next()){
        		String name = rset.getString("place_name");
        		rset.close();
        		conn.commit();
        		return name;
        	}else{
        		rset.close();
        		conn.commit();
        		return null;
        	}
        }catch (Exception e) {
        	try{
        		if(conn != null){
        		conn.rollback();
        		conn.close();
        		}
        	}catch(SQLException e1){
        		throw new OlympicsDBException("SQL Rollback getResidenceName", e1);
        	}
            throw new OlympicsDBException("Error checking member type", e);
        }
    }


    //////////  Events  //////////

    /**
     * Get all of the events listed in the olympics for a given sport
     *
     * @param sportId the ID of the sport we are filtering by
     * @return List of the events for that sport
     * @throws OlympicsDBException
     */
    ArrayList<HashMap<String, Object>> getEventsOfSport(Integer sportId) throws OlympicsDBException {
    	ArrayList<HashMap<String, Object>> events = new ArrayList<>();
    	Connection conn = null;
        try {
          conn = getConnection();
          conn.setTransactionIsolation(8);
          
          conn.setAutoCommit(false);

          String query = "SELECT * FROM Sport NATURAL JOIN Event JOIN Place ON (place_id  = sport_venue) "
          		+ "WHERE sport_id= ?"
          		+ " ORDER BY event_id;";
          PreparedStatement stmt = conn.prepareStatement(query);
          stmt.setInt(1, sportId);
          ResultSet rset = stmt.executeQuery();
          while (rset.next() ) {
        	  HashMap<String,Object> event = new HashMap<String,Object>();
              event.put("event_id", rset.getInt("event_id"));
              event.put("sport_id", rset.getInt("sport_id"));
              event.put("event_name", rset.getString("event_name"));
              event.put("event_gender", rset.getString("event_gender"));
              event.put("sport_venue", rset.getString("place_name"));
              
              java.sql.Timestamp timestamp = (rset.getTimestamp("event_start"));
              Date time = new Date(timestamp.getTime());
              
              event.put("event_start", time);
              events.add(event);
          }
          rset.close();
          conn.commit();
          conn.close();       
      }catch (Exception e) {
		  try{
			  if(conn != null){
				  conn.rollback();
				  conn.close();
			  }
		  }catch(SQLException e1){
			  throw new OlympicsDBException("SQL Rollback exception", e1);
		  }
          throw new OlympicsDBException("Error getting events details", e);
      }
      return events;
    }
    
    ArrayList<HashMap<String, Object>> getAdvEventsOfSport(Integer sportId, Integer offset, Integer limit) throws OlympicsDBException {
    	if (offset < 0) {
    		offset = 0;
        }
        if (limit < 0) {
            limit = 0;
        }
        
    	ArrayList<HashMap<String, Object>> events = new ArrayList<>();
    	Connection conn = null;
        try {
          conn = getConnection();
          conn.setTransactionIsolation(8);
          
          conn.setAutoCommit(false);

          String query = "SELECT * FROM Sport NATURAL JOIN Event JOIN Place ON (place_id  = sport_venue) "
          		+ "WHERE sport_id= ?"
          		+ " ORDER BY event_id ASC LIMIT ? OFFSET  ?;";
          PreparedStatement stmt = conn.prepareStatement(query);
          stmt.setInt(1, sportId);
          stmt.setInt(2, limit);
          stmt.setInt(3, offset);
          ResultSet rset = stmt.executeQuery();
          while (rset.next() ) {
        	  HashMap<String,Object> event = new HashMap<String,Object>();
              event.put("event_id", rset.getInt("event_id"));
              event.put("sport_id", rset.getInt("sport_id"));
              event.put("event_name", rset.getString("event_name"));
              event.put("event_gender", rset.getString("event_gender"));
              event.put("sport_venue", rset.getString("place_name"));
              
              java.sql.Timestamp timestamp = (rset.getTimestamp("event_start"));
              Date time = new Date(timestamp.getTime());
              
              event.put("event_start", time);
              events.add(event);
          }
          rset.close();
          conn.commit();
          conn.close();
      }catch (Exception e) {
    	  try{
			  if(conn != null){
				  conn.rollback();
				  conn.close();
			  }
		  }catch(SQLException e1){
			  throw new OlympicsDBException("SQL Rollback exception", e1);
		  }
          throw new OlympicsDBException("Error getting events details", e);
      }
      return events;
    }
    

    /**
     * Retrieve the results for a single event
     * @param eventId the key of the event
     * @return a hashmap for each result in the event.
     * @throws OlympicsDBException
     */
    ArrayList<HashMap<String, Object>> getResultsOfEvent(Integer eventId) throws OlympicsDBException {
    	ArrayList<HashMap<String, Object>> results = new ArrayList<>();
    	Connection conn = null;
    	try {
    		conn = getConnection();
            conn.setTransactionIsolation(8);
            
            conn.setAutoCommit(false);

    		if(checkEventType(conn, eventId)){
    			String query = "SELECT team_name AS participant, country_name, "
    					+ "CASE WHEN medal = 'G' THEN 'Gold' "
    					+ "WHEN medal = 'S' THEN 'Silver' "
    					+ "WHEN medal = 'B' THEN 'Bronze' "
    					+ "END AS medal "
    					+ "FROM Team NATURAL JOIN Event NATURAL JOIN Country "
    					+ "WHERE event_id = ? "
    					+ "ORDER BY participant;";
                PreparedStatement stmt = conn.prepareStatement(query);
                stmt.setInt(1, eventId);
                ResultSet rset = stmt.executeQuery();
                while (rset.next() ) {
    	        	HashMap<String,Object> result = new HashMap<String,Object>();
    	        	result.put("participant", rset.getString("participant"));
    	        	result.put("country_name", rset.getString("country_name"));
    	        	result.put("medal", rset.getString("medal"));
    	
    	            results.add(result);
                }
                rset.close();
                conn.commit();
                conn.close();
    		}else{
    			String query = "SELECT family_name || ', ' || given_names AS participant, "
	              + "country.country_name, case when medal = 'G' then 'Gold' "
	              + "when medal = 'S' then 'Silver' "
	              + "when medal = 'B' then 'Bronze' "
	              + "end "
	              + "AS medal "
	              + "FROM Member JOIN Participates ON (member_id = athlete_id) NATURAL JOIN country "
	              + "WHERE event_id = ? "
	              + "ORDER BY participant;";
	            PreparedStatement stmt = conn.prepareStatement(query);
	            stmt.setInt(1, eventId);
	            ResultSet rset = stmt.executeQuery();
	            while (rset.next() ) {
	              HashMap<String,Object> result = new HashMap<String,Object>();
	              result.put("participant", rset.getString("participant"));
	              result.put("country_name", rset.getString("country_name"));
	              result.put("medal", rset.getString("medal"));
	              
	              results.add(result);
	            }
		        rset.close();
		        conn.commit();
		        conn.close();
    		}    
    	} catch (Exception e) {
    		try{
    			if(conn != null){
    				conn.rollback();
    				conn.close();
    			}
    		}catch(SQLException e1){
    			throw new OlympicsDBException("SQL Rollback exception", e1);
    		}
    		throw new OlympicsDBException("Error getting events results", e);
    	}
    	return results;
    }
    
    private boolean checkEventType(Connection conn, Integer eventId) throws OlympicsDBException{
    	try{
    		String query = "SELECT 1 FROM Event NATURAL JOIN Team WHERE event_id=?";
    		PreparedStatement stmt = conn.prepareStatement(query);
    		stmt.setInt(1, eventId);
    		ResultSet rset = stmt.executeQuery();
    		if(rset.next()){
    			rset.close();
    			conn.commit();
    			return true;
    		}else{
    			rset.close();
    			conn.commit();
    			return false;
    		}
    	}catch (Exception e) {
    		try{
    			if(conn != null){
    				conn.rollback();
    				conn.close();
    			}
    		}catch(SQLException e1){
    			throw new OlympicsDBException("SQL Rollback exception", e1);
    		}
            throw new OlympicsDBException("Error checking event type", e);
        }
    }

    ///////   Journeys    ////////

    private int findPlaceId(Connection conn, String place) throws OlympicsDBException{
    	try{
    		String query = "SELECT place_id FROM Place WHERE place_name = ?";
    		PreparedStatement stmt = conn.prepareStatement(query);
    		stmt.setString(1, place);
    		ResultSet rset = stmt.executeQuery();
    		if(rset.next()){
    			int place_id = rset.getInt("place_id");
    			rset.close();
    			conn.commit();
    			return place_id;
    		}else{
    			rset.close();
    			conn.commit();
    			return 0;
    		}
    	}catch (Exception e) {
    		try{
    			if(conn != null){
    				conn.rollback();
    				conn.close();
    			}
    		}catch(SQLException e1){
    			throw new OlympicsDBException("SQL Rollback FindPlace_id", e1);
    		}
            throw new OlympicsDBException(e.getMessage());
        }
    }
    
    
    /**
     * Array list of journeys from one place to another on a given date
     * @param journeyDate the date of the journey
     * @param fromPlace the origin, starting place.
     * @param toPlace the destination, place to go to.
     * @return a list of all journeys from the origin to destination
     */
    
    ArrayList<HashMap<String, Object>> findJourneys(String fromPlace, String toPlace, Date journeyDate) throws OlympicsDBException {
    	ArrayList<HashMap<String, Object>> journeys = new ArrayList<HashMap<String, Object>>();
    	Connection conn = null;
        try{
        	conn = getConnection();
            conn.setTransactionIsolation(8);
            
            conn.setAutoCommit(false);

        	//details of journey information
        	String query = "SELECT journey_id, depart_time, arrive_time, from_place, to_place, vehicle_code, capacity - nbooked AS available_seats "
              + "FROM Journey JOIN Vehicle USING (vehicle_code)" + 
        			" WHERE depart_time BETWEEN ? AND ?"
        			+ " AND from_place = ? AND to_place = ?;";

        	Calendar c = Calendar.getInstance();
        	c.setTime(journeyDate);
        	c.add(Calendar.DATE, 1);
        	Date newDate = new Date(c.getTimeInMillis());
        	
        	PreparedStatement stmt = conn.prepareStatement(query);
        	stmt.setTimestamp(1, new java.sql.Timestamp(journeyDate.getTime()));
        	stmt.setTimestamp(2, new java.sql.Timestamp(newDate.getTime()));
        	
        	stmt.setInt(3, findPlaceId(conn, fromPlace));
        	stmt.setInt(4, findPlaceId(conn, toPlace));
        	ResultSet rset = stmt.executeQuery();
	        while (rset.next()) {
	        	HashMap<String,Object> journey = new HashMap<String,Object>();
	        	journey.put("journey_id", rset.getInt("journey_id"));
	          	journey.put("vehicle_code", rset.getString("vehicle_code"));
	          	journey.put("origin_name", getResidenceName(conn, rset.getInt("from_place")));
	          	journey.put("dest_name", getResidenceName(conn, rset.getInt("to_place")));
	          	
	          	
	          	java.sql.Timestamp timestamp = rset.getTimestamp("depart_time");
	            Date time = new Date(timestamp.getTime());
	          	journey.put("when_departs", time);
	          	
	          	timestamp = rset.getTimestamp("arrive_time");
	          	time = new Date(timestamp.getTime());
	            journey.put("when_arrives", time);
	            
	            journey.put("available_seats", rset.getInt("available_seats"));
	            journeys.add(journey);
	        }
	        rset.close();
            conn.commit();
            conn.close();
            return journeys;
        }
        catch (Exception e) {
        	try{
    			if(conn != null){
    				conn.rollback();
    				conn.close();
    			}
    		}catch(SQLException e1){
    			throw new OlympicsDBException("SQL Rollback exception", e1);
    		}
            throw new OlympicsDBException(e.getMessage());
        }
    }
    
    //Recursive findJourney
    /*
    ArrayList<HashMap<String, Object>> findJourneys(String fromPlace, String toPlace, Date journeyDate) throws OlympicsDBException {
    	ArrayList<HashMap<String, Object>> journeys = new ArrayList<HashMap<String, Object>>();
    	Calendar c = Calendar.getInstance();
    	c.setTime(journeyDate);
    	c.add(Calendar.DATE, 1);
    	Date newDate = new Date(c.getTimeInMillis());
    	Connection conn = null;
    	
    	//Find Direct Journeys
    	try{
        	conn = getConnection();
            conn.setTransactionIsolation(8);
            
            conn.setAutoCommit(false);

        	//details of journey information
        	String query = "SELECT journey_id, depart_time, arrive_time, from_place, to_place, vehicle_code, capacity - nbooked AS available_seats "
              + "FROM Journey JOIN Vehicle USING (vehicle_code)" + 
        			" WHERE depart_time BETWEEN ? AND ?"
        			+ " AND from_place = ? AND to_place = ?;";
        	
        	PreparedStatement stmt = conn.prepareStatement(query);
        	stmt.setTimestamp(1, new java.sql.Timestamp(journeyDate.getTime()));
        	stmt.setTimestamp(2, new java.sql.Timestamp(newDate.getTime()));
        	
        	stmt.setInt(3, findPlaceId(fromPlace));
        	stmt.setInt(4, findPlaceId(toPlace));
        	ResultSet rset = stmt.executeQuery();
	        while (rset.next()) {
	        	HashMap<String,Object> journey = new HashMap<String,Object>();
	        	journey.put("journey_id", rset.getInt("journey_id"));
	          	journey.put("vehicle_code", rset.getString("vehicle_code"));
	          	journey.put("origin_name", getResidenceName(rset.getInt("from_place")));
	          	journey.put("dest_name", getResidenceName(rset.getInt("to_place")));
	          	journey.put("when_departs", rset.getTimestamp("depart_time"));
	            journey.put("when_arrives", rset.getTimestamp("arrive_time"));
	            journey.put("available_seats", rset.getInt("available_seats"));
	            journeys.add(journey);
	        }
            rset.close();
            
        }catch (Exception e) {
        	try{
    			if(conn != null){
    				conn.rollback();
    				conn.close();
    			}
    		}catch(SQLException e1){
    			throw new OlympicsDBException("SQL Rollback exception", e1);
    		}
            throw new OlympicsDBException("Error getting journey details", e);
        }
    	//Find indirect Journeys, if exist, list all of them
    	try{
    		String query = "SELECT journey_id, depart_time, arrive_time, from_place, to_place, vehicle_code, capacity - nbooked AS available_seats "
    				+ "FROM Journey JOIN Vehicle USING (vehicle_code) "
    				+ "WHERE from_place = ( "
    				+ "  WITH RECURSIVE AllJourney(tfrom, tto, hops, mid) AS( "
    				+ "    SELECT from_place, to_place, 0, '0' "
    				+ "    FROM Journey "
    				+ "    WHERE depart_time BETWEEN ? AND ? AND to_place = ? "
    				+ "    UNION "
    				+ "    SELECT from_place, tto, AllJourney.hops+1, Alljourney.mid || tfrom "
    				+ "    FROM AllJourney, Journey "
    				+ "    WHERE to_place=tfrom "
    				+ "  ) "
    				+ "  SELECT CAST(mid AS INT) "
    				+ "  FROM AllJourney "
    				+ "  WHERE mid != '0' "
    				+ ") "
    				+ "OR to_place = ( "
    				+ "  WITH RECURSIVE AllJourney(tfrom, tto, hops, mid) AS( "
    				+ "    SELECT from_place, to_place, 0, '0' "
    				+ "    FROM Journey "
    				+ "    WHERE depart_time BETWEEN ? AND ? AND to_place = ? "
    				+ "    UNION "
    				+ "    SELECT from_place, tto, AllJourney.hops+1, Alljourney.mid || tfrom "
    				+ "    FROM AllJourney, Journey "
    				+ "    WHERE to_place=tfrom "
    				+ "  ) "
    				+ "  SELECT CAST(mid AS INT) "
    				+ "  FROM AllJourney "
    				+ "  WHERE mid != '0' "
    				+ ") "
    				+ "AND depart_time BETWEEN ? AND ? "
    				+ "AND arrive_time <= ("
    				+ "  SELECT MAX(depart_time) "
    				+ "  FROM ( "
    				+ "    SELECT depart_time, from_place "
    				+ "    FROM Journey "
    				+ "    WHERE from_place = ( "
    				+ "      WITH RECURSIVE AllJourney(tfrom, tto, hops, mid) AS( "
    				+ "        SELECT from_place, to_place, 0, '0' "
    				+ "        FROM Journey "
    				+ "        WHERE depart_time BETWEEN ? AND ? and to_place = ? "
    				+ "        UNION "
    				+ "        SELECT from_place, tto, AllJourney.hops+1, Alljourney.mid || tfrom "
    				+ "        FROM AllJourney, Journey "
    				+ "        WHERE to_place=tfrom "
    				+ "      ) "
    				+ "      SELECT CAST(mid AS INT)"
    				+ "      FROM AllJourney "
    				+ "      WHERE mid != '0' "
    				+ "    ) "
    				+ "  OR to_place =("
    				+ "      WITH RECURSIVE AllJourney(tfrom, tto, hops, mid) AS( "
    				+ "        SELECT from_place, to_place, 0, '0' "
    				+ "        FROM Journey "
    				+ "        WHERE depart_time BETWEEN ? AND ? and to_place = ? "
    				+ "        UNION "
    				+ "        SELECT from_place, tto, AllJourney.hops+1, Alljourney.mid || tfrom "
    				+ "        FROM AllJourney, Journey "
    				+ "        WHERE to_place=tfrom "
    				+ "      ) "
    				+ "      SELECT CAST(mid AS INT)"
    				+ "      FROM AllJourney "
    				+ "      WHERE mid != '0' "
    				+ "    ) "
    				+ "  ) AS Q "
    				+ "  WHERE Q.from_place != ?"
    				+ ") "
    				+ "ORDER BY depart_time; ";
    		PreparedStatement stmt = conn.prepareStatement(query);
    		stmt.setTimestamp(1, new java.sql.Timestamp(journeyDate.getTime()));
        	stmt.setTimestamp(2, new java.sql.Timestamp(newDate.getTime()));
        	stmt.setInt(3, findPlaceId(toPlace));
        	stmt.setTimestamp(4, new java.sql.Timestamp(journeyDate.getTime()));
        	stmt.setTimestamp(5, new java.sql.Timestamp(newDate.getTime()));
        	stmt.setInt(6, findPlaceId(toPlace));
        	stmt.setTimestamp(7, new java.sql.Timestamp(journeyDate.getTime()));
        	stmt.setTimestamp(8, new java.sql.Timestamp(newDate.getTime()));
        	stmt.setTimestamp(9, new java.sql.Timestamp(journeyDate.getTime()));
        	stmt.setTimestamp(10, new java.sql.Timestamp(newDate.getTime()));
        	stmt.setInt(11, findPlaceId(toPlace));
        	stmt.setTimestamp(12, new java.sql.Timestamp(journeyDate.getTime()));
        	stmt.setTimestamp(13, new java.sql.Timestamp(newDate.getTime()));
        	stmt.setInt(14, findPlaceId(toPlace));
        	stmt.setInt(15, findPlaceId(fromPlace));
        	ResultSet rset = stmt.executeQuery();
	        while (rset.next()) {
	        	HashMap<String,Object> journey = new HashMap<String,Object>();
	        	journey.put("journey_id", rset.getInt("journey_id"));
	          	journey.put("vehicle_code", rset.getString("vehicle_code"));
	          	journey.put("origin_name", getResidenceName(rset.getInt("from_place")));
	          	journey.put("dest_name", getResidenceName(rset.getInt("to_place")));
	          	journey.put("when_departs", rset.getTimestamp("depart_time"));
	            journey.put("when_arrives", rset.getTimestamp("arrive_time"));
	            journey.put("available_seats", rset.getInt("available_seats"));
	            journeys.add(journey);
	        }
            rset.close();
            conn.commit();
            conn.close();
    	}
        catch (Exception e) {
        	try{
    			if(conn != null){
    				conn.rollback();
    				conn.close();
    			}
    		}catch(SQLException e1){
    			throw new OlympicsDBException("SQL Rollback exception", e1);
    		}
            throw new OlympicsDBException("Error getting indirect journeys", e);
        }
        return journeys;
    }*/
    
    ArrayList<HashMap<String,Object>> getMemberBookings(String memberID) throws OlympicsDBException {
    	ArrayList<HashMap<String,Object>> bookings = new ArrayList<HashMap<String,Object>>();
        Connection conn = null;
    	try{
    		conn = getConnection();
            conn.setTransactionIsolation(8);
            
            conn.setAutoCommit(false);

    		String query = "SELECT * "
                 + "FROM Journey JOIN Booking USING (journey_id)" + " WHERE booked_for = ?"
                 		+ " ORDER BY depart_time DESC";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1,memberID);
            
            ResultSet rset = stmt.executeQuery();
            while (rset.next() ) {
            	HashMap<String,Object> booking = new HashMap<String,Object>();
            
	            booking.put("journey_id", rset.getInt("journey_id"));
	            booking.put("vehicle_code", rset.getString("vehicle_code"));
	            booking.put("origin_name", getResidenceName(conn, rset.getInt("from_place")));
	            booking.put("dest_name", getResidenceName(conn, rset.getInt("to_place")));
	            booking.put("when_departs", rset.getTimestamp("depart_time"));
	            booking.put("when_arrives", rset.getTimestamp("arrive_time"));
	            bookings.add(booking);
            }
            rset.close();
            conn.commit();
            conn.close();
            return bookings;
    	}
    	catch (Exception e) {
    		try{
    			if(conn != null){
    				conn.rollback();
    				conn.close();
    			}
    		}catch(SQLException e1){
    			throw new OlympicsDBException("SQL Rollback exception", e1);
    		}
            throw new OlympicsDBException("Error getting booking details", e);
        }
    }
                
    /**
     * Get details for a specific journey
     * 
     * @return Various details of journey - see JourneyDetails.java
     * @throws OlympicsDBException
     * @param journey_id
     */
    public HashMap<String,Object> getJourneyDetails(Integer jouneyId) throws OlympicsDBException {
    	HashMap<String,Object> details = new HashMap<String,Object>();
    	Connection conn = null;
        try{
        	conn = getConnection();
            conn.setTransactionIsolation(8);
            
            conn.setAutoCommit(false);

        	//details of journey
        	String query = "SELECT * "
                    + "FROM Journey JOIN Vehicle USING (vehicle_code) " + "WHERE journey_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1,jouneyId);
               
            ResultSet rset = stmt.executeQuery();
            while (rset.next() ) {
            	details.put("journey_id", rset.getInt("journey_id"));
            	details.put("vehicle_code", rset.getString("vehicle_code"));
            	details.put("origin_name", getResidenceName(conn, rset.getInt("from_place")));
            	details.put("dest_name", getResidenceName(conn, rset.getInt("to_place")));
            	details.put("when_departs", rset.getTimestamp("depart_time"));
            	details.put("when_arrives", rset.getTimestamp("arrive_time"));
             	details.put("capacity", rset.getInt("capacity"));
             	details.put("nbooked", rset.getInt("nbooked"));
            }
            rset.close();
            conn.commit();
            conn.close();
            return details;
        }
        catch (Exception e) {
        	try{
    			if(conn != null){
    				conn.rollback();
    				conn.close();
    			}
    		}catch(SQLException e1){
    			throw new OlympicsDBException("SQL Rollback exception", e1);
    		}
            throw new OlympicsDBException("Error getting journey details", e);
        }
    }
    
    public HashMap<String,Object> makeBooking(String byStaff, String forMember, String vehicle, Date departs) throws OlympicsDBException {
    	HashMap<String,Object> booking = new HashMap<String,Object>();
    	Connection conn = null;
    	try{
    		conn = getConnection();
            conn.setTransactionIsolation(8);

    		conn.setAutoCommit(false);

    		String query = "INSERT INTO Booking VALUES(?, ?, ?, ?);";
    		PreparedStatement stmt = conn.prepareStatement(query);
    		stmt.setString(1, forMember);
    		stmt.setString(2, byStaff);
    		stmt.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
    		stmt.setInt(4, findJourneyid(conn, vehicle, departs));
    		int linesInserted = stmt.executeUpdate();
    		
    		//On success
    		if(linesInserted == 1){
    			conn.commit();
    			query = "SELECT * FROM booking JOIN Journey USING (journey_id)"
        				+ " WHERE booked_for = ? AND booked_by = ? AND journey_id = ?";
        		stmt = conn.prepareStatement(query);
        		stmt.setString(1, forMember);
        		stmt.setString(2, byStaff);
        		stmt.setInt(3, findJourneyid(conn, vehicle, new Timestamp(departs.getTime())));
        		ResultSet rset = stmt.executeQuery();
        		if(rset.next()){
        			booking.put("vehicle", rset.getString("vehicle_code"));
        			booking.put("bookedfor_name", getMemberName(conn, forMember));
        			
        			Calendar c = Calendar.getInstance();
        			c.setTime(rset.getTimestamp("depart_time"));		
        			booking.put("when_departs", rset.getTimestamp("depart_time"));
        			booking.put("start_time", rset.getTimestamp("depart_time").getTime());
        			booking.put("when_arrives",rset.getTimestamp("arrive_time"));
        			
        			booking.put("dest_name", getResidenceName(conn, rset.getInt("to_place")));
        			booking.put("origin_name", getResidenceName(conn, rset.getInt("from_place")));
        			booking.put("bookedby_name", getMemberName(conn, rset.getString("booked_by")));
        			booking.put("when_booked", rset.getTimestamp("when_booked"));
        			conn.commit();
        			conn.close();
        		}else{
        			conn.close();
        			throw new SQLException("Error retriving booking submission page");
        		}
    		}else{
    			conn.rollback();
    			booking = null;
    			conn.close();
    		}	
    	}catch(SQLException e){
    		try{
    			if(conn != null){
    				conn.rollback();
    				conn.close();
    			}
    		}catch(SQLException e1){
    			throw new OlympicsDBException("SQL Rollback exception", e1);
    		}
    		return null;
    		//throw new OlympicsDBException(e.getMessage()); //"Cannot make booking, maybe you can ask a staff for help"
    	}catch(Exception e){
    		throw new OlympicsDBException(e.getMessage());
    	}
    	
    	return booking;
    }
    
    private String getMemberName(Connection conn, String memberID) throws OlympicsDBException{
    	try{
    		String query = "SELECT family_name, given_names FROM Member WHERE member_id = ?";
    		PreparedStatement stmt = conn.prepareStatement(query);
    		stmt.setString(1, memberID);
    		ResultSet rset = stmt.executeQuery();
    		if(rset.next()){
    			String memberName = rset.getString(1) + ", " + rset.getString(2);
    			rset.close();
    			conn.commit();
    			return memberName;
    		}else{
    			rset.close();
    			conn.rollback();
    	    	return null;
    		}
    	}catch(Exception e){
    		try{
    			if(conn != null){
    				conn.rollback();
    				conn.close();
    			}
    		}catch(SQLException e1){
    			throw new OlympicsDBException("SQL Rollback exception", e1);
    		}
    		throw new OlympicsDBException("Error fetching memberName", e);
    	}
    }
    
    private int findJourneyid(Connection conn, String vehicle, Date departs) throws OlympicsDBException{
    	try{
    		String query = "SELECT journey_id FROM Journey WHERE depart_time = ? AND vehicle_code = ?";
    		PreparedStatement stmt = conn.prepareStatement(query);
    		stmt.setTimestamp(1, new java.sql.Timestamp(departs.getTime()));
    		stmt.setString(2, vehicle);
    		ResultSet rset = stmt.executeQuery();
    		if(rset.next()){
    			int journey_id = rset.getInt(1);
    			rset.close();
    			conn.commit();
    			return journey_id;
    		}else{
    			rset.close();
    			conn.rollback();
    			return 0; //throw new OlympicsDBException("Sorry, did not find this journey");
    		}
    	}catch(Exception e){
    		try{
    			if(conn != null){
    				conn.rollback();
    				conn.close();
    			}
    		}catch(SQLException e1){
    			throw new OlympicsDBException("SQL Rollback exception", e1);
    		}
    		throw new OlympicsDBException("Error in finding Journeys", e);
    	}
    }
    
    public HashMap<String,Object> getBookingDetails(String memberID, Integer journeyId) throws OlympicsDBException {
    	HashMap<String,Object> booking = new HashMap<String,Object>();
    	Connection conn = null;
    	try{
    		conn = getConnection();
            conn.setTransactionIsolation(8);
            
            conn.setAutoCommit(false);

    		String query = "SELECT * FROM Booking JOIN Journey USING (journey_id)"
        			+ " WHERE booked_for = ? AND journey_id = ?";
        	PreparedStatement stmt = conn.prepareStatement(query);
        	stmt.setString(1, memberID);
        	stmt.setInt(2, journeyId);
        	ResultSet rset = stmt.executeQuery();
        	while(rset.next()){
        		booking.put("journey_id", journeyId);
        		booking.put("vehicle", rset.getString("vehicle_code"));
        		booking.put("when_departs", rset.getTimestamp("depart_time"));
        		booking.put("dest_name", getResidenceName(conn, rset.getInt("to_place")));
        		booking.put("origin_name", getResidenceName(conn, rset.getInt("from_place")));
        		booking.put("bookedby_name", getMemberName(conn, rset.getString("booked_by")));
        		booking.put("bookedfor_name", getMemberName(conn, rset.getString("booked_for")));
        		booking.put("when_booked", rset.getTimestamp("when_booked"));
        		booking.put("when_arrives", rset.getTimestamp("arrive_time"));
        	}
        	rset.close();
        	conn.commit();
        	conn.close();
        	
        	return booking;
    	}catch(Exception e){
    		try{
    			if(conn != null){
    				conn.rollback();
    				conn.close();
    			}
    		}catch(SQLException e1){
    			throw new OlympicsDBException("SQL Rollback exception", e1);
    		}
    		throw new OlympicsDBException("Error getting booking details", e);
    	}
    	
    }
    
	public ArrayList<HashMap<String, Object>> getSports() throws OlympicsDBException {
		ArrayList<HashMap<String,Object>> sports = new ArrayList<HashMap<String,Object>>();
		Connection conn = null;
		try {
			conn = getConnection();
            conn.setTransactionIsolation(8);
            
            conn.setAutoCommit(false);

            String query = "SELECT sport_id, sport_name, discipline "
            		+ "FROM Sport;";
            PreparedStatement stmt = conn.prepareStatement(query);
            ResultSet rset = stmt.executeQuery();
            while (rset.next() ) {
           	 	HashMap<String,Object> sport = new HashMap<String,Object>();
           	 	sport.put("sport_id", rset.getInt("sport_id"));
           		sport.put("sport_name", rset.getString("sport_name"));
           		sport.put("discipline", rset.getString("discipline"));
           		sports.add(sport);
            }
            rset.close();
            conn.commit();
            conn.close();
            return sports;
        } catch (Exception e) {
        	try{
    			if(conn != null){
    				conn.rollback();
    				conn.close();
    			}
    		}catch(SQLException e1){
    			throw new OlympicsDBException("SQL Rollback exception", e1);
    		}
            throw new OlympicsDBException("Error getting sports details", e);
        }
	}


    /////////////////////////////////////////
    /// Functions below don't need
    /// to be touched.
    ///
    /// They are for connecting and handling errors!!
    /////////////////////////////////////////

    /**
     * Default constructor that simply loads the JDBC driver and sets to the
     * connection details.
     *
     * @throws ClassNotFoundException if the specified JDBC driver can't be
     * found.
     * @throws OlympicsDBException anything else
     */
    DatabaseBackend(InputStream config) throws ClassNotFoundException, OlympicsDBException {
    	Properties props = new Properties();
    	try {
			props.load(config);
		} catch (IOException e) {
			throw new OlympicsDBException("Couldn't read config data",e);
		}

    	dbUser = props.getProperty("username");
    	dbPass = props.getProperty("userpass");
    	String port = props.getProperty("port");
    	String dbname = props.getProperty("dbname");
    	String server = props.getProperty("address");;
    	
        // Load JDBC driver and setup connection details
    	String vendor = props.getProperty("dbvendor");
		if(vendor==null) {
    		throw new OlympicsDBException("No vendor config data");
    	} else if ("postgresql".equals(vendor)) { 
    		Class.forName("org.postgresql.Driver");
    		connstring = "jdbc:postgresql://" + server + ":" + port + "/" + dbname;
    	} else if ("oracle".equals(vendor)) {
    		Class.forName("oracle.jdbc.driver.OracleDriver");
    		connstring = "jdbc:oracle:thin:@" + server + ":" + port + ":" + dbname;
    	} else throw new OlympicsDBException("Unknown database vendor: " + vendor);
		
		// test the connection
		Connection conn = null;
		try {
			conn = getConnection();
		} catch (SQLException e) {
			throw new OlympicsDBException("Couldn't open connection", e);
		} finally {
			reallyClose(conn);
		}
    }

	/**
	 * Utility method to ensure a connection is closed without 
	 * generating any exceptions
	 * @param conn Database connection
	 */
	private void reallyClose(Connection conn) {
		if(conn!=null)
			try {
				conn.close();
			} catch (SQLException ignored) {}
	}

    /**
     * Construct object with open connection using configured login details
     * @return database connection
     * @throws SQLException if a DB connection cannot be established
     */
    private Connection getConnection() throws SQLException {
        Connection conn;
        conn = DriverManager.getConnection(connstring, dbUser, dbPass);
        return conn;
    }

}
